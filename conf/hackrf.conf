[GNSS-SDR]

;######### GLOBAL OPTIONS ##################
GNSS-SDR.internal_fs_sps=2000000
; gnss-sdr-monitor
GNSS-SDR.telecommand_enabled=true
GNSS-SDR.telecommand_tcp_port=3333

;######### SIGNAL_SOURCE CONFIG ############
SignalSource.implementation=Osmosdr_Signal_Source
SignalSource.item_type=gr_complex
SignalSource.sampling_frequency=2000000
SignalSource.freq=1575420000
SignalSource.gain=40
SignalSource.rf_gain=40
SignalSource.if_gain=30
SignalSource.AGC_enabled=false
SignalSource.samples=0
SignalSource.repeat=false
;# Next line enables the internal HackRF One bias (3.3 VDC)
SignalSource.osmosdr_args=hackrf,bias=1
SignalSource.enable_throttle_control=false
SignalSource.dump=false
SignalSource.dump_filename=./signal_source.dat

;######### SIGNAL_CONDITIONER CONFIG ############
SignalConditioner.implementation=Signal_Conditioner

;######### DATA_TYPE_ADAPTER CONFIG ############
DataTypeAdapter.implementation=Pass_Through

;######### INPUT_FILTER CONFIG ############
InputFilter.implementation=Freq_Xlating_Fir_Filter
InputFilter.decimation_factor=1
InputFilter.input_item_type=gr_complex
InputFilter.output_item_type=gr_complex
InputFilter.taps_item_type=float
InputFilter.number_of_taps=5
InputFilter.number_of_bands=2
InputFilter.band1_begin=0.0
InputFilter.band1_end=0.85
InputFilter.band2_begin=0.9
InputFilter.band2_end=1.0
InputFilter.ampl1_begin=1.0
InputFilter.ampl1_end=1.0
InputFilter.ampl2_begin=0.0
InputFilter.ampl2_end=0.0
InputFilter.band1_error=1.0
InputFilter.band2_error=1.0
InputFilter.filter_type=bandpass
InputFilter.grid_density=16
InputFilter.dump=false
InputFilter.dump_filename=../data/input_filter.dat

;######### RESAMPLER CONFIG ############
Resampler.implementation=Pass_Through

;######### CHANNELS GLOBAL CONFIG ############
Channels_1C.count=8
Channels.in_acquisition=1

;######### ACQUISITION GLOBAL CONFIG ############
Acquisition_1C.implementation=GPS_L1_CA_PCPS_Acquisition
Acquisition_1C.item_type=gr_complex
Acquisition_1C.coherent_integration_time_ms=1
Acquisition_1C.pfa=0.01
Acquisition_1C.doppler_max=5000
Acquisition_1C.doppler_step=250
Acquisition_1C.max_dwells=1
Acquisition_1C.dump=false
Acquisition_1C.dump_filename=./acq_dump.dat

;######### TRACKING GLOBAL CONFIG ############
Tracking_1C.implementation=GPS_L1_CA_DLL_PLL_Tracking
Tracking_1C.item_type=gr_complex
Tracking_1C.extend_correlation_symbols=10
Tracking_1C.early_late_space_chips=0.5
Tracking_1C.early_late_space_narrow_chips=0.15
Tracking_1C.pll_bw_hz=40
Tracking_1C.dll_bw_hz=2.0
Tracking_1C.pll_bw_narrow_hz=5.0
Tracking_1C.dll_bw_narrow_hz=1.50
Tracking_1C.fll_bw_hz=10
Tracking_1C.enable_fll_pull_in=true
Tracking_1C.enable_fll_steady_state=false
Tracking_1C.dump=false
Tracking_1C.dump_filename=tracking_ch_

;######### TELEMETRY DECODER GPS CONFIG ############
TelemetryDecoder_1C.implementation=GPS_L1_CA_Telemetry_Decoder
TelemetryDecoder_1C.dump=false

;######### OBSERVABLES CONFIG ############
Observables.implementation=Hybrid_Observables
Observables.dump=false
Observables.dump_filename=./observables.dat

;######### PVT CONFIG ############
PVT.implementation=RTKLIB_PVT
PVT.positioning_mode=Single
;PVT.positioning_mode=Static
;PVT.positioning_mode=PPP_Kinematic
PVT.output_rate_ms=100
PVT.display_rate_ms=500
PVT.iono_model=Broadcast
;PVT.iono_model=Iono‐Free-LC
PVT.trop_model=Saastamoinen
PVT.flag_rtcm_server=true
PVT.flag_rtcm_tty_port=false
PVT.rtcm_dump_devname=/dev/pts/1
PVT.rtcm_tcp_port=2101
PVT.rtcm_MT1019_rate_ms=5000
PVT.rtcm_MT1077_rate_ms=1000
;PVT.rtcm_station_id=1111
PVT.rinex_version=2
; gnss-sdr-monitor
PVT.enable_monitor=true
PVT.monitor_client_addresses=192.168.110.69
;PVT.monitor_client_addresses=127.0.0.1_192.168.110.69
;PVT.monitor_client_addresses=127.0.0.1
PVT.monitor_udp_port=1111
;PVT.nmea_dump_filename=./gnss_sdr_pvt.nmea
PVT.flag_nmea_tty_port=true
PVT.nmea_dump_devname=/dev/pts/11
;PVT.nmea_dump_devname=/dev/ptmx
;PVT.dynamics_model=1
;PVT.enable_rx_clock_correction=false
;PVT.max_clock_offset_ms=40
;PVT.code_phase_error_ratio_l1=100
;PVT.raim_fde=0
;PVT.reject_GPS_IIA=1
;PVT.phwindup=1
;PVT.earth_tide=1
;PVT.output_enabled=true
;PVT.rtcm_output_file_enabled=false
;PVT.gpx_output_enabled=false
;PVT.geojson_output_enabled=false
;PVT.kml_output_enabled=false
;PVT.xml_output_enabled=false
;PVT.rinex_output_enabled=false
;PVT.nmea_output_file_enabled=false
;PVT.rtcm_MT1045_rate_ms=5000
;PVT.rtcm_MSM_rate_ms=1000
;PVT.rtcm_MT1097_rate_ms=1000
;PVT.output_path=./
;PVT.dump=false
;PVT.dump_filename=./pvt.dat
;PVT.enable_monitor_ephemeris=true
;PVT.monitor_ephemeris_client_addresses=127.0.0.1
;PVT.monitor_ephemeris_udp_port=1234
;PVT.enable_protobuf=false


;######### MONITOR CONFIG ############
Monitor.enable_monitor=true
Monitor.decimation_factor=1
Monitor.client_addresses=192.168.110.69
;Monitor.client_addresses=127.0.0.1
Monitor.udp_port=1112

